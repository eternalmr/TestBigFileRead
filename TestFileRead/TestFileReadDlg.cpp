
// TestFileReadDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "TestFileRead.h"
#include "TestFileReadDlg.h"
#include "afxdialogex.h"
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <sstream>
#include <regex>

using std::cout;
using std::endl;
using std::string;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

const int IMAX = 1000;
const int JMAX = 1000;

// 用于应用程序"关于"菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestFileReadDlg 对话框



CTestFileReadDlg::CTestFileReadDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TESTFILEREAD_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestFileReadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
}

BEGIN_MESSAGE_MAP(CTestFileReadDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CTestFileReadDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTestFileReadDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CTestFileReadDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CTestFileReadDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CTestFileReadDlg::OnBnClickedButton5)
END_MESSAGE_MAP()


// CTestFileReadDlg 消息处理程序

BOOL CTestFileReadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将"关于..."菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	OnBnClickedButton1();
	m_progress.SetRange(0, 100);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CTestFileReadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CTestFileReadDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CTestFileReadDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestFileReadDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	AfxOleInit();
	HRESULT hr;
	hr = m_pConnection.CreateInstance("ADODB.Connection");
	m_pConnection->ConnectionTimeout = 8;
	try
	{
		_bstr_t strConnect = "Provider = SQLOLEDB.1; Persist Security Info = False; User ID = sa; Password = 123; Initial Catalog = TestBigFile; Data Source = DESKTOP-CR";//初始化连接  
		hr = m_pConnection->Open(strConnect, "", "", adModeUnknown);//打开数据库
		if (FAILED(hr))
		{
			MessageBox(_T("Fail!"));
			return;
		}
		else
			MessageBox(_T("Success!"));
	}
	catch (_com_error e)
	{
		AfxMessageBox(e.Description());
		return;
	}
}

BOOL CTestFileReadDlg::ExcuteCmd(CString bstrSqlCmd)//执行SQL语句
{
	CTestFileReadDlg ct;
	auto m_sqlCmd = _bstr_t(bstrSqlCmd);
	_variant_t RecordsAffected;

	try {
		m_pRecordset = m_pConnection->Execute(m_sqlCmd, &RecordsAffected, adCmdText);
	}
	catch (_com_error e) {
		AfxMessageBox(e.ErrorMessage());
		return FALSE;
	}
	return TRUE;
}

void CTestFileReadDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CString sql;
	sql.Format(_T("delete from [TestBigFile].[dbo].[Products]"));
	ExcuteCmd(sql);

	//从文件读数据
	std::fstream f;
	f.open("result.txt", std::ios::in);

	//如果知道数据格式，可以直接用>>读入
	int line_num;
	std::string str;
	int price;

	clock_t startTime, endTime;
	startTime = clock();//计时开始
	m_progress.SetPos(0);

	int count = 1;

	while (f >> line_num >> str >> price)
	{
		//sql.Format(_T("insert into [TestBigFile].[dbo].[Products]([prod_id],[prod_name],[prod_price]) values(%d, '%s', %d)"), line_num, CString("apple4"), price);

		//if (!ExcuteCmd(sql)) {
		//	AfxMessageBox(_T("Failed"));
		//	break;
		//}

		//m_progress.SetPos((int)(100*count/(IMAX*JMAX)));
		//count++;
	}
	AfxMessageBox(_T("Success"));
	endTime = clock();//计时结束

	sql.Format(_T("The run time is: %lf s"), (double)(endTime - startTime) / CLOCKS_PER_SEC);
	MessageBox(sql);
}


void CTestFileReadDlg::OnBnClickedButton3()
{
	// 按照 [prod_id], [prod_name], [prod_price] 的格式写文件

	std::ofstream outfile;   //输出流

	outfile.open("result.txt", std::ios::out);   //每次写都定位的文件结尾，不会丢失原来的内容，用out则会丢失原来的内容
	if (!outfile.is_open())
	{
		cout << "Open file failure" << endl;
	}

	std::string str;

	int count = 1;

	for (int i = 0; i != IMAX; ++i)
	{
		for (int j = 0; j != JMAX; ++j)
		{
			str = "test_" + std::to_string(i) + "_" + std::to_string(j);
			outfile << count << "\t" << str << "\t" << j << endl;  //在result.txt中写入结果
			m_progress.SetPos((int)(100*count / (IMAX*JMAX)));
			count++;
		}
	}
	outfile.close();
	AfxMessageBox(_T("Write completed!"));
}


void CTestFileReadDlg::OnBnClickedButton4()
{
	std::ofstream outfile;   //输出流

	outfile.open("BigFile2.txt", std::ios::out);   //每次写都定位的文件结尾，不会丢失原来的内容，用out则会丢失原来的内容
	if (!outfile.is_open())
	{
		cout << "Open file failure" << endl;
	}

	std::string str;

	int count = 1;

	clock_t startTime, endTime;
	startTime = clock();//计时开始

	for (int i = 0; i != IMAX; ++i)
	{
		for (int j = 0; j != JMAX; ++j)
		{
			str = "test_" + std::to_string(i) + "_" + std::to_string(j);
			//outfile << count << str << j << endl;  //在result.txt中写入结果
			outfile << count << "\t" << str << "\t" << j << "\n";  //在result.txt中写入结果
			m_progress.SetPos((int)(100 * count / (IMAX*JMAX)));
			count++;
		}
	}
	endTime = clock();//计时结束
	CString time_delta_str;
	time_delta_str.Format(_T("The run time is: %lf s"), (double)(endTime - startTime) / CLOCKS_PER_SEC);
	MessageBox(time_delta_str);

	outfile.close();
	AfxMessageBox(_T("Write completed!"));
}


std::vector<string> split_string(const string& in, const string& delim)
{
	std::regex re{ delim };
	return std::vector<string> {
		std::sregex_token_iterator(in.begin(), in.end(), re, -1),
			std::sregex_token_iterator()
	};
}


int splitStringToVect(const string & srcStr, std::vector<string> & destVect, const string & strFlag)
{
	int pos = srcStr.find(strFlag, 0);
	int startPos = 0;
	int splitN = pos;
	string lineText(strFlag);

	while (pos > -1)
	{
		//lineText = srcStr.substr(startPos, splitN);
		//destVect.push_back(lineText);
		destVect.emplace_back(srcStr.substr(startPos, splitN));
		startPos = pos + strFlag.length();
		pos = srcStr.find(strFlag, pos + strFlag.length());
		splitN = pos - startPos;
	}

	lineText = srcStr.substr(startPos, srcStr.length() - startPos);
	if(lineText.length()>0)
		destVect.push_back(lineText);	

	return destVect.size();
}

void CTestFileReadDlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	int method = 3;

	clock_t start = clock();

	switch (method)
	{
	case 1: {
		std::string buf;

		std::ifstream fin("BigFile.txt");

		while (fin >> buf);

		fin.close();
		break;
	}
	case 2: {
		std::ifstream fin("BigFile.txt");

		std::stringstream buf;
		buf << fin.rdbuf();

		fin.close();
		break;
	}
	case 3: {
		std::ifstream fin("BigFile2.txt", std::ios::binary);

		auto size = fin.seekg(0, std::ios::end).tellg();

		std::vector<char> buf(size); //初始化buf
		fin.seekg(0, std::ios::beg).read(&buf[0], static_cast<std::streamsize>(buf.size()));

		std::string str(buf.data(), size);


		//std::vector<string> vec_str = split_string(str, "\r\n");

		std::vector<string> vec_str;

		splitStringToVect(str, vec_str, "\r\n");

		while ()
		{
		}


		fin.close();
		break;
	}
	case 4: {
		std::ifstream fin("BigFile2.txt", std::ios::binary);
		std::vector<std::string> tt;
		std::string test;
		while (getline(fin, test))
		{
			tt.push_back(test);
		}

		//for (int i = 0; i < tt.size(); i++)
		//{
		//	cout << tt[i] << endl;  //打印数据
		//}
		fin.close();
		break;
	}
	case 5: {
		std::ifstream fin("BigFile.txt", std::ios::binary);

		fin.seekg(0); //go to begining
		auto size = fin.seekg(0, std::ios::end).tellg();

		std::vector<char> buf(size); //初始化buf
		fin.seekg(0, std::ios::beg).read(&buf[0], static_cast<std::streamsize>(buf.size()));

		std::string str(buf.data(), size);

		fin.close();
		break;
	}
	default:
		break;
	}

	clock_t end = clock();
	CString time_delta_str;
	time_delta_str.Format(_T("The run time is: %lf s"), (double)(end - start) / CLOCKS_PER_SEC);
	MessageBox(time_delta_str);
}
